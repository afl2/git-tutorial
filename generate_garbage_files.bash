#!/bin/bash

for n in 1 2 3 4 5 6 7 8 9 10
do
    file=$(cat /dev/urandom | tr -cd 'a-f0-9' | head -c 32)
    touch ${file}.txt
done